var myApp = angular.module("myApp", ['ui.router','HomeModule'])

.config(['$stateProvider', '$urlRouterProvider','$locationProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl:'home.html',
                leftNavFlag : false
            })   
            .state('ml', {
                url: '/ml',
                templateUrl:'ml.html',
            })
            .state('ai', {
                url: '/ai',
                templateUrl:'ai.html',
            })
            .state('datascience', {
                url: '/datascience',
                templateUrl:'datascience.html',
            })
            .state('bigDataHadoop', {
                url: '/bigDataHadoop',
                templateUrl:'bigDataHadoop.html',
            })
            .state('bigDataPython', {
                url: '/bigDataPython',
                templateUrl:'bigDataPython.html',
            })
            .state('bigDataAWS', {
                url: '/bigDataAWS',
                templateUrl:'bigDataAWS.html',
            })        
            .state('devopsAWS', {
                url: '/devopsAWS',
                templateUrl:'devopsAWS.html',
            })
            .state('ui', {
                url: '/ui',
                templateUrl:'ui.html',
            })
            .state('rpa', {
                url: '/rpa',
                templateUrl:'rpa.html',
            })
            .state('java', {
                url: '/java',
                templateUrl:'java.html',
            })
            .state('python', {
                url: '/python',
                templateUrl:'python.html',
            })
            .state('mobile', {
                url: '/mobile',
                templateUrl:'mobile.html',
            })
            .state('hybris', {
                url: '/hybris',
                templateUrl:'hybris.html',
            })
            .state('blockchain', {
                url: '/blockchain',
                templateUrl:'blockchain.html',
            })
            .state('iot', {
                url: '/iot',
                templateUrl:'iot.html',
            })
            .state('sap', {
                url: '/sap',
                templateUrl:'sap.html',
            })
            .state('erp', {
                url: '/erp',
                templateUrl:'arp.html',
            })
            .state('salesforce', {
                url: '/salesforce',
                templateUrl:'salesforce.html',
            })
            .state('sf', {
                url: '/sf',
                templateUrl:'sf.html',
            })
        }
])

angular.module('HomeModule', []).controller('homeController', ['$scope','$rootScope', function($scope,$rootScope) {
    $rootScope.selectedCourse = "";
    $scope.selectCourse = function(course){
         $rootScope.selectedCourse = course;
    }

}]);
